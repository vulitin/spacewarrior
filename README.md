# My semestral project for A4M033KAJ 'SpaceWarrior' README

version 1.0:
    - added structure of the application
    - added jQuery to project dependencies

version 1.1
    - added player and enemies

version 1.2
    - added interaction between players <-> bullets and player <-> enemies

version 1.3
    - added responsive behavior