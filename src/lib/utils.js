import { style } from 'lib/style.js';

const mixins = {
    addMenuOption: function (text, callback, className) {

        // use the className argument, or fallback to menuConfig, but
        // if menuConfig isn't set, just use "default"
        className || (className = this.menuConfig.className || 'default');

        // set the x coordinate to game.world.center if we use "center"
        // otherwise set it to menuConfig.startX
        const x = this.menuConfig.startX === "center" ?
            this.game.world.centerX :
            this.menuConfig.startX;

        // set Y coordinate based on menuconfig
        const y = this.menuConfig.startY;

        // create text
        const txt = this.game.add.text(
            x,
            (this.optionCount * 80) + y,
            text,
            style.navitem[className]
        );
        txt.useHandCursor = true;
        // use the anchor method to center if startX set to center.
        txt.anchor.setTo(this.menuConfig.startX === "center" ? 0.5 : 0.0);

        txt.inputEnabled = true;

        txt.events.onInputUp.add(callback);
        txt.events.onInputOver.add((target) => {
            target.setStyle(style.navitem.hover);
        });
        txt.events.onInputOut.add((target) => {
            target.setStyle(style.navitem[className]);
        });

        this.optionCount++;
    }
};

function displayScore(score, wp) {
    // remove previous text from parent
    wp.empty();
    // find div where to place score
    const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.setAttribute('xlink','http://www.w3.org/1999/xlink');
    svg.setAttribute('width','180');
    svg.setAttribute('height','60');

    const text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    text.setAttribute('x', '15');
    text.setAttribute('y', '30');
    text.setAttribute('fill', '#bf4926');
    text.setAttribute('transform', 'scale(2)');
    text.textContent = score;

    svg.appendChild(text);
    wp.append(svg);
}

function playLifeLostAnimation(lives) {
    const livesArr = $('#lives .life');
    $(livesArr).each((ind, life) => {
        if (ind == lives - 1)
            $(life).addClass('lost');
    })
}

function restoreLives() {
    const livesArr = $('#lives .life');
    $(livesArr).each((ind, life) => {
        if ($(life).hasClass('lost'))
            $(life).removeClass('lost');
    })
}

export { mixins, displayScore, playLifeLostAnimation, restoreLives };