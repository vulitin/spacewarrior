import { mixins, displayScore } from 'lib/utils.js';

class Menu extends Phaser.State {

    preload() {
        this.game.load.image('menu-bg', 'images/bg/menu-bg.jpg');
    }

    init() {
        this.titleText = this.game.make.text(this.game.world.centerX, 100, "Space Warrior", {
            font: 'bold 60pt TheMinion',
            fill: '#FDFFB5',
            align: 'center'
        });

        this.titleText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.titleText.anchor.set(0.5);
        this.optionCount = 1;

        // menu position
        this.menuConfig = {
            startY: 200,
            startX: this.game.width/2 - this.game.width/16
        };

        // set scores
        this.setScoreTitles();

        // load best score from Local Storage
        this.game.bestScore = JSON.parse(localStorage.getItem('bestScore')) || 0;
        displayScore(this.game.bestScore, this.game.wpBestScore);

        // score
        this.game.score = 0;
        displayScore(this.game.score, this.game.wpScore);
    }

    create() {
        // Hide lives
        $('div.score:last-child').hide();
        $('#lives').hide();
        //  A spacey background
        this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'menu-bg');
        this.game.add.existing(this.titleText);

        this.addMenuOption('Start', () => {
            this.game.state.start("Game");
        });
    }

    setScoreTitles() {
        this.game.wpScore = $('#svg-score');
        this.game.wpBestScore = $('#svg-best-score');
    }
}

Phaser.Utils.mixinPrototype(Menu.prototype, mixins);

export default Menu;
