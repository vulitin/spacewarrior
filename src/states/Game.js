import { mixins, displayScore, playLifeLostAnimation, restoreLives } from 'lib/utils.js';
import SpaceShip from 'objects/SpaceShip';
import EnemyShipBig from 'objects/EnemyShipBig';
import EnemyShipSmall from 'objects/EnemyShipSmall';
import EnemyBullet from 'objects/EnemyBullet';
import GameOver from 'states/GameOver'

class Game extends Phaser.State {

    init() {
        // player settings
        this.playerName = "player";
        this.playerBullet = "bullet";
        this.playerSpeed = 200;
        this.playerBulletTime = 0;
        this.reviveTime = 0;
        this.playerBulletInterval = 200; // can shoot every 200ms
        this.lives = 3;

        // enemy settings
        this.enemyBullet = "enemyBullet";
        this.enemyBig = "enemyBig";
        this.enemySmall = "enemySmall";
        this.enemyBulletTime = 0;

        // misc
        this.nextSpawn = 0;

        // settings for pause menu
        this.menuConfig = {
            startY: -70,
            startX: this.game.width - 200
        };

        // set default score at the beginning of the game
        this.game.score = 0;
        displayScore(this.game.score, this.game.wpScore);
    }

    preload() {
        this.game.load.image('space', 'images/bg/deep-space.jpg');
        this.game.load.image(this.playerName, 'images/ships/ship.png');
        this.game.load.image(this.playerBullet, 'images/objects/bullets.png');
        this.game.load.image(this.enemyBullet, 'images/objects/enemy-bullet.png');
        this.game.load.spritesheet(this.enemyBig, 'images/ships/invader32x32x4.png', 32, 32);
        this.game.load.image(this.enemySmall, 'images/ships/invader.png');

        // sounds
        this.game.load.audio('playerShoot', 'sounds/blaster.mp3');
        this.game.load.audio('enemyShoot', 'sounds/enemy.wav');
        this.game.load.audio('enemyDeath', 'sounds/enemy_death.wav');

        this.optionCount = 1; // for pause menu
    }

    create() {
        // Show lives
        $('div.score:last-child').show();
        $('#lives').css('display', 'flex');
        restoreLives();

        this.stage.disableVisibilityChange = false;
        //  A space background
        this.spaceBG = this.game.add.tileSprite(0, 0, this.game.width, this.game.height, 'space');
        //  Our ship bullets
        this.playerBullets = this.game.add.group();
        this.setBullets(this.playerBullets, this.playerBullet, Phaser.Sprite);
        //  Enemy ships bullets
        this.enemyBullets = this.game.add.group();
        this.setBullets(this.enemyBullets, this.enemyBullet, EnemyBullet);

        //  Our player ship
        this.player = new SpaceShip(this.game, this.game.width/2, this.game.height - 100, this.playerName);
        this.player.body.collideWorldBounds = true;
        this.playerBlaster = this.game.add.audio('playerShoot');
        // hack for tween animation of players death
        this.deathAnim = this.game.add.tween(this.player).to({alpha: 1}, 0, Phaser.Easing.Linear.None, true, 0, 0, true);

        // Create enemies group
        this.enemies = this.game.add.group();
        this.enemies.enableBody = true;
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyShoot = this.game.add.audio('enemyShoot');
        this.enemyDeath = this.game.add.audio('enemyDeath');

        //  Game input
        this.cursors = this.game.input.keyboard.createCursorKeys();
        this.fireButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.addMenuOption('Pause', () => {
            this.game.paused = true;

            // Add hint text how to unpause
            this.choiseLabel = this.game.add.text(this.game.width/2, this.game.height-150, 'Click to continue',
                { font: '36pt TheMinion', fill: '#fff' });
            this.choiseLabel.anchor.setTo(0.5, 0.5);
        });

        // here is onClick listener for unpausing game
        this.game.input.onDown.add(this.unpause.bind(this), self);
    }

    unpause() {
        if(this.game.paused) {
            // destroy unpause text
            this.choiseLabel.destroy();
            // Unpause the game
            this.game.paused = false;
        }
    }

    update() {
        //  Scroll the background
        this.spaceBG.tilePosition.y += 1;

        // Player controls
        this.playerMoves();

        // Player shot
        if (this.fireButton.isDown)
        {
            this.fireBullet();
        }

        // Enemy is shooting
        if (this.game.time.now > this.enemyBulletTime)
        {
            this.enemyFires();
        }

        // move enemies
        this.moveEnemies();

        // create new enemy
        this.createNewEnemy();

        //  Check collisions
        this.game.physics.arcade.overlap(this.playerBullets, this.enemies, this.enemyIsShot, null, this);
        if (this.game.time.now > this.reviveTime) {
            this.game.physics.arcade.overlap(this.enemyBullets, this.player, this.playerIsShot, null, this);
            this.game.physics.arcade.overlap(this.enemies, this.player, this.playerCollidesEnemy, null, this);
        }
    }

    playerMoves() {
        // Reset the player, then check for movement keys
        this.player.body.velocity.setTo(0, 0);

        if (this.cursors.left.isDown)
        {
            this.player.body.velocity.x = -this.playerSpeed;
        }
        if (this.cursors.right.isDown)
        {
            this.player.body.velocity.x = this.playerSpeed;
        }
        if (this.cursors.up.isDown) {
            this.player.body.velocity.y = -this.playerSpeed;
        }
        if (this.cursors.down.isDown) {
            this.player.body.velocity.y = this.playerSpeed;
        }
    }

    setBullets(bullets, name, classType) {
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.classType = classType;
        bullets.createMultiple(40, name);
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 0.5);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);
    }

    fireBullet() {
        if (this.game.time.now > this.playerBulletTime)
        {
            const bullet = this.playerBullets.getFirstExists(false);
            if (bullet)
            {
                bullet.reset(this.player.body.x + 16, this.player.body.y + 16);
                bullet.lifespan = 2000;
                bullet.rotation = this.player.rotation;
                this.game.physics.arcade.velocityFromRotation(this.player.rotation, 400, bullet.body.velocity);
                this.playerBulletTime = this.game.time.now + this.playerBulletInterval;
                if (!this.playerBlaster.isPlaying) {
                    this.playerBlaster.play();
                }
            }
        }
    }

    getAliveEnemies() {
        let aliveEnemies = [];
        this.enemies.forEachAlive(function(enemy){
            // put every alive enemy in an array
            aliveEnemies.push(enemy);
        });
        return aliveEnemies;
    }

    moveEnemies() {
        let aliveEnemies = this.getAliveEnemies();

        aliveEnemies.forEach((enemy) => {
            enemy.descend();
            this.screenDestroy(enemy);
        });
    }

    enemyFires() {
        //  Grab the first bullet we can from the pool
        const enemyBullet = this.enemyBullets.getFirstExists(false);

        let aliveEnemies = this.getAliveEnemies();

        if (enemyBullet && aliveEnemies.length > 0)
        {
            const ind = this.game.rnd.integerInRange(0, aliveEnemies.length-1);
            // randomly select one of them
            const shooter = aliveEnemies[ind];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
            enemyBullet.setTargetPosition(this.player.x, this.player.y);
            // next shoot will be between 1s and 3s
            this.enemyBulletTime = this.game.time.now + this.game.rnd.integerInRange(500, 1500);

            if (!this.enemyShoot.isPlaying) {
                this.enemyShoot.play();
            }
        }
    }

    screenDestroy(obj) {
        // check if enemy left game field
        if (obj.y > this.game.height) {
            obj.kill();
        }
    }

    createNewEnemy() {
        if (this.game.time.now > this.nextSpawn) {
            this.enemies.add(this.getRandomEnemy());
            // call the next enemy for random between 2 and 4 seconds
            this.nextSpawn = this.game.time.now + this.game.rnd.integerInRange(500, 2000);
        }
    }

    getRandomEnemy() {
        // 1 - small fast enemy, 2 - big slow enemy
        const enemyType = this.game.rnd.integerInRange(1, 2);
        if (enemyType === 1) {
            return new EnemyShipSmall(this.game,
                this.game.rnd.integerInRange(50, this.game.width - 100),
                0,
                this.enemySmall,
                this.game.rnd.integerInRange(50, 100));
        }
        else {
            return new EnemyShipBig(this.game,
                this.game.rnd.integerInRange(50, this.game.width - 100),
                0,
                this.enemyBig,
                this.game.rnd.integerInRange(50, 100));
        }
    }

    enemyIsShot(bullet, enemy) {
        //  Increase the score
        this.getScore(enemy);

        //  When a bullet hits an enemy we destroy them both
        bullet.kill();
        enemy.kill();
        if (!this.enemyDeath.isPlaying) {
            this.enemyDeath.play();
        }
    }

    playerIsShot(player, bullet) {
        // destroy bullet, play players death animation and subtract one life
        bullet.kill();
        this.playDeathAnimation(player);
        this.loseLife();
    }

    playerCollidesEnemy(player, enemy) {
        // destroy enemy, play players death animation and subtract one life
        this.getScore(enemy);
        enemy.kill();

        this.playDeathAnimation(player);
        this.loseLife();
    }

    clearGame() {
        // clear game field
        this.player.kill();
        this.enemies.destroy(true);
        this.enemyBullets.destroy(true);
        this.playerBullets.destroy(true);
    }

    playDeathAnimation(player) {
        // players death animation, just blinking for 1s
        if (!this.deathAnim.isRunning) {
            this.deathAnim = this.game.add.tween(player).to({alpha: 0}, 1000, Phaser.Easing.Linear.None, true, 0, 0, true);
            this.reviveTime = this.game.time.now + 1100;
        }
    }

    loseLife() {
        // subtract one players life
        playLifeLostAnimation(this.lives);
        this.lives--;

        if (this.lives < 1) {
            this.clearGame();
            this.game.state.start("GameOver");
        }
    }

    getScore(enemy) {
        // add score from dead enemy
        this.game.score += enemy.getScore();
        displayScore(this.game.score, this.game.wpScore);
        if (this.game.score > this.game.bestScore) {
            displayScore(this.game.score, this.game.wpBestScore);
        }
    }
}

Phaser.Utils.mixinPrototype(Game.prototype, mixins);

export default Game;