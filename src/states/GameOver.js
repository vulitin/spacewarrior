import { mixins } from 'lib/utils.js'

class GameOver extends Phaser.State {

    init() {
        this.menuConfig = {
            startY: 160,
            startX: this.game.width/2  - this.game.width/6
        };
    }

    preload() {
        this.game.load.image('gameover-bg', 'images/bg/gameover-bg.png');

        this.optionCount = 1;
    }

    create() {
        localStorage.setItem('bestScore', JSON.stringify(this.game.score));

        this.game.add.sprite(0, 0, 'gameover-bg');
        const titleStyle = { font: 'bold 60pt TheMinion', fill: '#FDFFB5', align: 'center'};
        const text = this.game.add.text(this.game.world.centerX, 100, "Game Over", titleStyle);
        text.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        text.anchor.set(0.5);

        this.addMenuOption('Play Again', () => {
            this.game.state.start("Game");
        });
        this.addMenuOption('Main Menu', () => {
            this.game.state.start("Menu");
        })
    }
}

Phaser.Utils.mixinPrototype(GameOver.prototype, mixins);

export default GameOver;