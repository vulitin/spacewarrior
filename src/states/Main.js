import Menu from 'states/Menu';
import Game from 'states/Game';
import GameOver from 'states/GameOver';

class Main extends Phaser.State {

    loadFonts() {
        const WebFontConfig = {
            custom: {
                families: ['TheMinion'],
                urls: ['/styles/theminion.css']
            }
        };
        WebFont.load(WebFontConfig);
    }

    addGameStates() {
        this.game.state.add("Menu", Menu);
        this.game.state.add("Game", Game);
        this.game.state.add("GameOver", GameOver);
    }

    loadBgm() {
        this.game.load.audio('main', 'music/cyborg_ninja.mp3');
    }

    addGameMusic() {
        this.music = this.game.add.audio('main');
        this.music.loop = true;
        this.music.play();
    }

    preload() {
        this.loadFonts();
        this.loadBgm();
    }

    create() {
        // global scaling settings
        this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.game.scale.refresh();

        // add states and bg music
        this.addGameStates();
        this.addGameMusic();
        //  This will run in Canvas mode, so let's gain a little speed and display
        this.game.renderer.clearBeforeRender = false;
        this.game.renderer.roundPixels = true;
        //  We need arcade physics
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        setTimeout(() => {
            this.game.state.start("Menu");
        }, 0);
    }
}

export default Main;