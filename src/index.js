import Main from 'states/Main';

class Game extends Phaser.Game {

	constructor() {
	    const gameFiled = $("#game-field");
		super(gameFiled.width(), gameFiled.height(), Phaser.CANVAS, 'game-field', null);
		this.state.add('Main', Main, false);
		this.state.start('Main');
	}

}

new Game();
