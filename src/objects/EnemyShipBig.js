class EnemyShipBig extends Phaser.Sprite {

    constructor(game, x, y, name, shift) {
        super(game, x, y, name);

        this.anchor.setTo(0.5, 0.5);
        this.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
        this.play('fly');
        this.game.physics.enable(this, Phaser.Physics.ARCADE);

        this.body.moves = false;
        this.body.drag.set(100);
        this.body.maxVelocity.set(100);

        const dur = 1000;
        game.add.tween(this).to({
            x: shift + this.x
        }, dur, Phaser.Easing.Circular.In0ut, true, 0, -1, true);

        game.stage.addChild(this);

        // score
        this.score = 50;
    }

    descend() {
        this.y += 0.7;
    }

    getScore() {
        return this.score;
    }
}

export default EnemyShipBig;