class EnemyShipSmall extends Phaser.Sprite {

    constructor(game, x, y, name, shift) {
        super(game, x, y, name);

        this.anchor.setTo(0.5, 0.5);
        this.game.physics.enable(this, Phaser.Physics.ARCADE);

        this.body.drag.set(100);
        this.body.maxVelocity.set(300);

        const dur = 1000;
        game.add.tween(this).to({
            x: this.x + shift
        }, dur, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);

        game.stage.addChild(this);

        // score
        this.score = 100;
    }

    descend() {
        this.y += 1;
    }

    getScore() {
        return this.score;
    }
}

export default EnemyShipSmall;