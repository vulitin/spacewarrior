class EnemyBullet extends Phaser.Sprite{

    constructor(game, x, y, bulletName) {
        super(game, x,  y, bulletName);

        // Set the pivot point for this sprite to the center
        this.anchor.setTo(0.5, 0.5);

        // Enable physics on the missile
        this.game.physics.enable(this, Phaser.Physics.ARCADE);

        // Define constants that affect motion
        this.SPEED = 250; // missile speed pixels/second
        this.TURN_RATE = 5; // turn rate in degrees/frame
        this.WOBBLE_LIMIT = 15; // degrees
        this.WOBBLE_SPEED = 250; // milliseconds

        // Create a variable called wobble that tweens back and forth between
        // -this.WOBBLE_LIMIT and +this.WOBBLE_LIMIT forever
        this.wobble = this.WOBBLE_LIMIT;
        this.game.add.tween(this)
            .to(
                { wobble: -this.WOBBLE_LIMIT },
                this.WOBBLE_SPEED, Phaser.Easing.Sinusoidal.InOut, true, 0,
                Number.POSITIVE_INFINITY, true
            );
    }

    update() {
        this.targetAngle = this.game.math.angleBetween(
            this.x, this.y,
            this.targetX, this.targetY
        );
        // Add our "wobble" factor to the targetAngle to make the missile wobble
        // Remember that this.wobble is tweening (above)
        this.targetAngle += this.game.math.degToRad(this.wobble);

        // Gradually (this.TURN_RATE) aim the missile towards the target angle
        if (this.rotation !== this.targetAngle) {
            // Calculate difference between the current angle and targetAngle
            let delta = this.targetAngle - this.rotation;

            // Keep it in range from -180 to 180 to make the most efficient turns.
            if (delta > Math.PI) delta -= Math.PI * 2;
            if (delta < -Math.PI) delta += Math.PI * 2;

            if (delta > 0) {
                // Turn clockwise
                this.angle += this.TURN_RATE;
            } else {
                // Turn counter-clockwise
                this.angle -= this.TURN_RATE;
            }

            // Just set angle to target angle if they are close
            if (Math.abs(delta) < this.game.math.degToRad(this.TURN_RATE)) {
                this.rotation = this.targetAngle;
            }
        }

        // Calculate velocity vector based on this.rotation and this.SPEED
        this.body.velocity.x = Math.cos(this.rotation) * this.SPEED;
        this.body.velocity.y = Math.sin(this.rotation) * this.SPEED;
    }

    setTargetPosition(x, y) {
        const m = (y - this.y)/(x - this.x);
        if (this.y < y) {
            this.targetX = this.game.height / m + x;
            this.targetY = y + this.game.height;
        } else {
            this.targetX = -this.game.height / m + x;
            this.targetY = y - this.game.height;
        }
    }
}

export default EnemyBullet;