class SpaceShip extends Phaser.Sprite {

    constructor(game, x, y, name) {
        super(game, x, y, name);

        this.anchor.set(0.5);
        this.game.physics.enable(this, Phaser.Physics.ARCADE);

        this.angle -= 90;
        this.body.drag.set(100);
        this.body.maxVelocity.set(200);

        game.stage.addChild(this);
    }
}

export default SpaceShip;